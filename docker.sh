#!/bin/bash

# Building BotFather
./gradlew clean assembleDist

# Unpacking Distrib
cd build
rm -fr docker
mkdir -p docker
cd distributions
rm -fr actor-bots
unzip actor-bots.zip
cp -r actor-bots/* ../docker/
cd ../..

# Building docker
docker build -t 360ground/meda-sticker-bot:1.0.0 .