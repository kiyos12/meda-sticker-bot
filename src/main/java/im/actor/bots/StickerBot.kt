package im.actor.bots

import im.actor.bots.framework.MagicForkScope
import im.actor.bots.framework.stateful.*
import java.awt.AlphaComposite
import java.awt.Dimension
import java.awt.image.BufferedImage
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.util.*
import javax.imageio.ImageIO

class StickerBot(scope: MagicForkScope) : MagicStatefulBot(scope) {
    override fun configure() {
        // Configure group behaviour
        println("Stickerbot loaded")
        ownNickname = "stickers"
        enableInGroups = true
        onlyWithMentions = false
        var stickerPackId = -1
        var emojiText = ""

        // Sticker Pack IDs
        // General Collection: 642605919 - This is also the default collection
        // Arada Collection: 1035352861
        // Celebrity Collection: 551477612
        // Chickita Collection: 762383959
        // AbeKebe Collection: 1894709003

        oneShot("/start") {
            sendText("Hi, i'm a sticker bot!")
        }

        oneShot("/pack") {
            checkIfAdmin(scope.peer.id)

            val stickerPackId = createStickerPack(scope.peer.id);

            sendText("Sticker pack created: " + stickerPackId);
        }

        oneShot("/list") {
            //            checkIfAdmin(scope.peer.id)

            val stickerPacks = showStickerPacks(scope.peer.id);

            sendText(stickerPacks.toString())

        }

        oneShot("/make") {
            //            checkIfAdmin(scope.peer.id)

//            makeStickerPackDefault(scope.peer.id, 642605919)
//            makeStickerPackDefault(scope.peer.id, 1035352861)
//            makeStickerPackDefault(scope.peer.id, 551477612)
//            makeStickerPackDefault(scope.peer.id, 762383959)
//            makeStickerPackDefault(scope.peer.id, 1894709003)

            makeStickerPackDefault(scope.peer.id, 581311641)
            makeStickerPackDefault(scope.peer.id, 1544556136)
            makeStickerPackDefault(scope.peer.id, 480011450)
            makeStickerPackDefault(scope.peer.id, 157591737)
            makeStickerPackDefault(scope.peer.id, 1594619859)

            sendText("Default pack set!")
        }

        oneShot("default") {
            if (isText) {
                sendText(text)
            }
            if (isPhoto) {

                val file = getFile(doc.fileId(), doc.accessHash())

                val image = ImageIO.read(ByteArrayInputStream(file))

                val imageDimension = Dimension(image.width, image.height)
                var image128Dimension = getScaledDimension(imageDimension, Dimension(128, 128))
                var image256Dimension = getScaledDimension(imageDimension, Dimension(256, 256))

                //128 image size
                val img128 = BufferedImage(image128Dimension.width, image128Dimension.height, BufferedImage.TYPE_INT_ARGB)
                val g128 = img128.createGraphics()
                g128.setComposite(AlphaComposite.Src)
                g128.drawImage(image, 0, 0, image128Dimension.width, image128Dimension.height, null)
                g128.dispose()

                val baos128 = ByteArrayOutputStream()
                ImageIO.write(img128, "png", baos128)
                baos128.flush()
                val image128 = baos128.toByteArray()
                baos128.close()


                //128 image size
                val img256 = BufferedImage(image256Dimension.width, image256Dimension.height, BufferedImage.TYPE_INT_ARGB)
                val g256 = img256.createGraphics()
                g256.setComposite(AlphaComposite.Src)
                g256.drawImage(image, 0, 0, image256Dimension.width, image256Dimension.height, null)
                g256.dispose()

                val baos256 = ByteArrayOutputStream()
                ImageIO.write(img256, "png", baos256)
                baos256.flush()
                val image256 = baos256.toByteArray()
                baos256.close()


                if (true/*userIsAdmin(scope.peer.id)*/) {
                    val file = getFile(doc.fileId(), doc.accessHash())
                    sendText("File" + doc.name() + " File Ext: " + doc.ext)
                    addSticker(scope.peer.id, 551477612, Optional.of(":smile:"), image128, image128Dimension.width, image128Dimension.height, image256, image256Dimension.width, image256Dimension.height, file, 512, 512)
                } else {
                    sendText("Only admin users can send images to me! አልሰማህም ስልህ！")
                }
//                addSticker( scope.peer.id, 10, ":smile:", image128 = )
                //addSticker()
            }
        }

        raw("/createpack") {
            before {
                stickerPackId = createStickerPack(scope.peer.id)
                if (stickerPackId != -1) {
                    sendText("Sticker pack created: " + stickerPackId + "\n Start sending your stickers.\nNow send me the Emoji")
                    goto("stickeradd")
                }
            }
            expectInput("stickeradd") {
                received {
                    emojiText = text
                    sendText("Now send your png image 512x512.")
                    goto("recievesticker")
                }
                validate {
                    if (!isText) {
                        sendText("Please, send valid text message including an emoji string :smile:")
                        return@validate false
                    }
                    return@validate true
                }
            }
            expectInput("recievesticker") {
                received {
                    val file = getFile(doc.fileId(), doc.accessHash())

                    val image = ImageIO.read(ByteArrayInputStream(file))

                    val imageDimension = Dimension(image.width, image.height)
                    var image128Dimension = getScaledDimension(imageDimension, Dimension(128, 128))
                    var image256Dimension = getScaledDimension(imageDimension, Dimension(256, 256))

                    //128 image size
                    val img128 = BufferedImage(image128Dimension.width, image128Dimension.height, BufferedImage.TYPE_INT_ARGB)
                    val g128 = img128.createGraphics()
                    g128.setComposite(AlphaComposite.Src)
                    g128.drawImage(image, 0, 0, image128Dimension.width, image128Dimension.height, null)
                    g128.dispose()

                    val baos128 = ByteArrayOutputStream()
                    ImageIO.write(img128, "png", baos128)
                    baos128.flush()
                    val image128 = baos128.toByteArray()
                    baos128.close()


                    //128 image size
                    val img256 = BufferedImage(image256Dimension.width, image256Dimension.height, BufferedImage.TYPE_INT_ARGB)
                    val g256 = img256.createGraphics()
                    g256.setComposite(AlphaComposite.Src)
                    g256.drawImage(image, 0, 0, image256Dimension.width, image256Dimension.height, null)
                    g256.dispose()

                    val baos256 = ByteArrayOutputStream()
                    ImageIO.write(img256, "png", baos256)
                    baos256.flush()
                    val image256 = baos256.toByteArray()
                    baos256.close()


                    if (true/*userIsAdmin(scope.peer.id)*/) {
                        val file = getFile(doc.fileId(), doc.accessHash())
                        sendText("File" + doc.name() + " File Ext: " + doc.ext)
                        addSticker(scope.peer.id, stickerPackId, Optional.of(emojiText), image128, image128Dimension.width, image128Dimension.height, image256, image256Dimension.width, image256Dimension.height, file, 512, 512)
                    } else {
                        sendText("Only admin users can send images to me! አልሰማህም ስልህ！")
                    }

                    sendText("Your sticker was added. To add another send emoji text.")
                    goto("stickeradd")
                }
                validate {
                    if (!isPhoto) {
                        sendText("Please, send valid png image.")
                        return@validate false
                    }
                    return@validate true
                }
            }
        }

        raw("/addsticker") {
            before {
                val stickerPacks = showStickerPacks(scope.peer.id);
                sendText("These are your packs choose the one " + stickerPacks.toString())
                goto("selectpack")
            }
            expectInput("selectpack") {
                received {
                    stickerPackId = text.toInt();
                    sendText("Pack selected.\nYou can send stickers.\nSend me the Emoji")
                    goto("stickeradd")
                }
                validate {
                    try {
                        val stickerPackId = text.toInt();
                        if (!showStickerPacks(scope.peer.id).contains(stickerPackId.toString())) {
                            sendText("Unknown sticker pack ID");
                            return@validate false;
                        } else {
                            return@validate true
                        }
                    } catch (nfe: NumberFormatException) {
                        sendText("The thing you insert doesn't seem like a sticker pack ID");
                        return@validate false;
                    }
                    return@validate true;
                }
            }
            expectInput("stickeradd") {
                received {
                    emojiText = text
                    sendText("Now send your png image 512x512.")
                    goto("recievesticker")
                }
                validate {
                    if (!isText) {
                        sendText("Please, send valid text message including an emoji string :smile:")
                        return@validate false
                    }
                    return@validate true
                }
            }
            expectInput("recievesticker") {
                received {
                    val file = getFile(doc.fileId(), doc.accessHash())

                    val image = ImageIO.read(ByteArrayInputStream(file))

                    val imageDimension = Dimension(image.width, image.height)
                    var image128Dimension = getScaledDimension(imageDimension, Dimension(128, 128))
                    var image256Dimension = getScaledDimension(imageDimension, Dimension(256, 256))

                    //128 image size
                    val img128 = BufferedImage(image128Dimension.width, image128Dimension.height, BufferedImage.TYPE_INT_ARGB)
                    val g128 = img128.createGraphics()
                    g128.setComposite(AlphaComposite.Src)
                    g128.drawImage(image, 0, 0, image128Dimension.width, image128Dimension.height, null)
                    g128.dispose()

                    val baos128 = ByteArrayOutputStream()
                    ImageIO.write(img128, "png", baos128)
                    baos128.flush()
                    val image128 = baos128.toByteArray()
                    baos128.close()


                    //128 image size
                    val img256 = BufferedImage(image256Dimension.width, image256Dimension.height, BufferedImage.TYPE_INT_ARGB)
                    val g256 = img256.createGraphics()
                    g256.setComposite(AlphaComposite.Src)
                    g256.drawImage(image, 0, 0, image256Dimension.width, image256Dimension.height, null)
                    g256.dispose()

                    val baos256 = ByteArrayOutputStream()
                    ImageIO.write(img256, "png", baos256)
                    baos256.flush()
                    val image256 = baos256.toByteArray()
                    baos256.close()


                    if (true/*userIsAdmin(scope.peer.id)*/) {
                        val file = getFile(doc.fileId(), doc.accessHash())
                        sendText("File " + doc.name() + " File Ext: " + doc.ext)
                        addSticker(scope.peer.id, stickerPackId, Optional.of(emojiText), image128, image128Dimension.width, image128Dimension.height, image256, image256Dimension.width, image256Dimension.height, file, 512, 512)
                    } else {
                        sendText("Only admin users can send images to me! አልሰማህም ስልህ！")
                    }

                    sendText("Your sticker was added. To add another send emoji text.")
                    goto("stickeradd")
                }
                validate {
                    if (!isPhoto) {
                        sendText("Please, send valid png image.")
                        return@validate false
                    }
                    return@validate true
                }
            }

        }

    }

    fun checkIfAdmin(userId: Int) {
        if (!userIsAdmin(userId)) {
            sendText("Only admin users can send images to me! አልሰማህም ስልህ！")
        }
    }

    fun getScaledDimension(imgSize: Dimension, boundary: Dimension): Dimension {
        val original_width = imgSize.width
        val original_height = imgSize.height
        val bound_width = boundary.width
        val bound_height = boundary.height
        var new_width = original_width
        var new_height = original_height

        // first check if we need to scale width
        if (original_width > bound_width) {
            //scale width to fit
            new_width = bound_width;
            //scale height to maintain aspect ratio
            new_height = (new_width * original_height) / original_width;
        }

        // then check if we need to scale even with the new height
        if (new_height > bound_height) {
            //scale height to fit instead
            new_height = bound_height;
            //scale width to maintain aspect ratio
            new_width = (new_height * original_width) / original_height;
        }

        return Dimension(new_width, new_height)
    }
}